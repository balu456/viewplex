'use strict';

// Use UMD so that this data can be both on the tour itself
// and on the script which generates index.html from index.html.tpl

(function (factory) {
  if (typeof exports === 'object' && typeof require === 'function') {
    module.exports = factory();
  } else {
    window.APP_DATA = factory();
  }
}(function() {
return {
  "name": "Sample Tour",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": true,
    "viewControlButtons": true
  },
  "scenes": [
    {
      "id": "red",
      "name": "This is a scene with a red color",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "size": 512,
          "tileSize": 512
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": Math.PI/2
      },
      "linkHotspots": [
        {
          "yaw": 0,
          "pitch": -0.2,
          "rotation": 0,
          "target": "green"
        },
        {
          "yaw": 0.34532802058046386,
          "pitch": -0.8048572066075046,
          "rotation": 0.7853981633974483,
          "target": "blue"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.2,
          "pitch": 0,
          "title": "Larger title in a hotspot which is pretty large",
          "text": "A large text in a large hotspot large text in a large hotspot large text in a large hotspot&nbsp;large text in a large hotspot.<div><br></div><div>With line breaks and everything!</div><div>With line breaks and everything!</div><div>With line breaks and everything!</div><div>With line breaks and everything!</div><div>With line breaks and everything!</div><div>With line breaks and everything!</div><div>With line breaks and everything!</div><div>With line breaks and everything!</div><div>With line breaks and everything!</div><div>With line breaks and everything!</div>"
        },
        {
          "yaw": -0.8389213739286028,
          "pitch": 0.6275970749030826,
          "title": "Title",
          "text": "Text"
        }
      ]
    },
    {
      "id": "green",
      "name": "Green",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "size": 512,
          "tileSize": 512
        }
      ],
      "faceSize": 512,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": Math.PI/2
      },
      "linkHotspots": [],
      "infoHotspots": []
    },
    {
      "id": "blue",
      "name": "Blue",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "size": 512,
          "tileSize": 512
        }
      ],
      "faceSize": 512,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": Math.PI/2
      },
      "linkHotspots": [],
      "infoHotspots": []
    }
  ]
};
}));